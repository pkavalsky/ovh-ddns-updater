FROM node:12.10.0

ENV DDNS_DOMAIN ''
ENV DDNS_USERNAME ''
ENV DDNS_PASSWORD ''
ENV INTERVAL ''

RUN npm install -g pkg
RUN mkdir /var/app
ADD . /var/app

WORKDIR /var/app
RUN chmod 777 -R /var/app

CMD /var/app/bin/run.sh

