
### For development:
### 1. run container 
```docker-compose up -d```
### 2. enter inside container
```docker exec -it node-ddns /bin/bash```
### 3. watch typescript changes
```npm run tsc:w``` 
### 4. to execute 
```./lib/index.js ```
### 5. create build:
```
sh bin/build.sh
```
