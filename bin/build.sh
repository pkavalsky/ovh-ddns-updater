npm run tsc && \
mv lib/index.js lib/ovh-ddns-updater && \
pkg lib/ovh-ddns-updater --output ./build/ovh-ddns-updater-linux --t=linux && \
pkg lib/ovh-ddns-updater --output ./build/ovh-ddns-updater-windows --t=windows
