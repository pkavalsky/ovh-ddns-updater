#!/usr/bin/env node
import * as prog from 'caporal';
import {container} from "./services/services"
import {TYPES} from "./services/types";

prog.version("1.0.0");

prog.logger(container.get(TYPES.LoggerInterface))

container
  .getAll<CommandInterface>(TYPES.CommandInterface)
  .forEach((command: CommandInterface) => {
  command
    .configure(prog)
    .action((args, options, logger) => {
      command.execute(args, options, logger)
    })
});
prog.parse(process.argv);

