import {inject, injectable} from "inversify"
import {TYPES} from "../services/types"
import {LoggerInterface} from "../Logger/LoggerInterface"
import {DdnsIpUpdater} from "../Ddns/DdnsIpUpdater"

@injectable()
export class DdnsUpdateManager {

  private readonly NO_IP = 'NO_IP'

  constructor(
    @inject(TYPES.DdnsIpUpdater) private ddnsIpUpdater: DdnsIpUpdater,
    @inject(TYPES.LoggerInterface) private logger: LoggerInterface
  ) {}

  public initialize(domain: string, username: string, password: string, interval: number) : void
  {
    let lastIp : string = this.NO_IP;
    let lastFinished : boolean = true;

    setInterval(async () => {

      if(lastFinished) {
        this.logger.info(`Starting new ddns update process...`);
        lastFinished = false;
        try {
          lastIp = await this.ddnsIpUpdater.process(domain, username, password, lastIp);
        } catch (error) {
          lastIp = this.NO_IP;
          this.logger.error(`${error}`);
        }
        lastFinished = true;
      }

    }, interval*1000);

  }
}
