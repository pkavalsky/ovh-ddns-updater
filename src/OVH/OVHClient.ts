import axios from 'axios';
import {injectable} from "inversify"

@injectable()
export class OVHClient {

  private readonly URL : string = "http://www.ovh.com/nic/update?system=dyndns"

  public async updateDdnsIp(ip : string, domain: string, username: string, password: string) : Promise<any>
  {
    return axios.get(`${this.URL}&hostname=${domain}&myip=${ip}`, {
      auth: {
        username: username,
        password: password
      },
    });
  }
}
