

interface CommandInterface {

  configure(prog : Caporal) : Command;

  execute(args: { [k: string]: any }, options: { [k: string]: any }, logger: Logger) : void

}
