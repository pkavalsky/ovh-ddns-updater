import {inject, injectable} from "inversify"
import {TYPES} from "../services/types"
import {DdnsUpdateManager} from "../Manager/DdnsUpdateManager"

@injectable()
export class DdnsIpUpdateCommand implements CommandInterface {

  constructor(
    @inject(TYPES.DdnsUpdateManager) private ddnsUpdateManager: DdnsUpdateManager
  ) {}

  public configure(prog : Caporal) : Command
  {
    return prog
      .command('update', 'Update ddns ip at OVH')
      .argument('<domain>', 'Ddns domain', prog.STRING)
      .argument('<username>', 'Ddns domain username', prog.STRING)
      .argument('<password>', 'Ddns domain password', prog.STRING)
      .argument('<interval>', 'How often should ip change be checked', prog.INT)
  }

  public execute(args: { [k: string]: any }, options: { [k: string]: any }, logger: Logger) : void
  {
    this.ddnsUpdateManager.initialize(args.domain, args.username, args.password, args.interval);
  }
}
