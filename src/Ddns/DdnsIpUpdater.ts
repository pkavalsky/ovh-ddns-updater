import {inject, injectable} from "inversify"
import {Ip} from "../Ipify/Model/Ip"
import {TYPES} from "../services/types"
import {OVHClient} from "../OVH/OVHClient"
import {IpifyClient} from "../Ipify/IpifyClient"
import {LoggerInterface} from "../Logger/LoggerInterface"

@injectable()
export class DdnsIpUpdater {

  constructor(
    @inject(TYPES.OVHClient) private ovhClient: OVHClient,
    @inject(TYPES.IpifyClient) private ipifyClient: IpifyClient,
    @inject(TYPES.LoggerInterface) private logger: LoggerInterface
  ) {}

  public async process(domain: string, username: string, password: string, lastIp : string) : Promise<string>
  {
    this.logger.info(`Calling ipify to get public ip address...`);
    let ip : Ip = await this.ipifyClient.getPublicIp();
    this.logger.info(`Public ip is: ${ip.get()}`);

    if(ip.get() === lastIp) {
      this.logger.info(`No ddns ip update is needed`);
      return lastIp;
    }

    let newIp = ip.get();
    this.logger.info(`New ip spotted: ${ip.get()}. Updating current ip from ${lastIp} to ${newIp}...`);
    this.logger.info(`Calling ovh to change ddns...`);
    let ovhResult : any = await this.ovhClient.updateDdnsIp(newIp, domain, username, password);
    this.logger.info(`OVH response:`);
    console.log(ovhResult.data);
    return newIp;
  }
}
