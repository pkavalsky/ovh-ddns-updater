export class Ip {

  private readonly ip : string;

  constructor(ip: string)
  {
    this.validate(ip);
    this.ip = ip;
  }

  public get() : string
  {
    return this.ip;
  }

  private validate(ip: string) : void
  {
    if (! /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {
      throw new Error("Invalid ip address");
    }
  }
}
