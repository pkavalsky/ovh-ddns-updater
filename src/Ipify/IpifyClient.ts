import axios from 'axios';
import {injectable} from "inversify"
import {Ip} from "./Model/Ip"
import * as _ from "lodash";

@injectable()
export class IpifyClient {

  private readonly URL = 'https://api.ipify.org?format=json';

  public async getPublicIp()
  {
    let result = await axios.get(this.URL)
    if(!_.get(result, "data.ip")) {
      throw Error("Invalid ipify response")
    }
    return new Ip(result.data.ip);
  }
}
