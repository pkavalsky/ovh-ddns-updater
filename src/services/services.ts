import { Container } from "inversify";
import { TYPES } from "./types";
import "reflect-metadata";
import {DdnsIpUpdateCommand} from "../Command/DdnsIpUpdateCommand"
import {OVHClient} from "../OVH/OVHClient"
import {IpifyClient} from "../Ipify/IpifyClient"
import {LoggerInterface} from "../Logger/LoggerInterface"
import {DdnsUpdateManager} from "../Manager/DdnsUpdateManager"
import {LoggerFactory} from "../Logger/LoggerFactory"
import {DdnsIpUpdater} from "../Ddns/DdnsIpUpdater"

const container = new Container();

const logger = LoggerFactory.create();

//commands
container.bind<CommandInterface>(TYPES.CommandInterface).to(DdnsIpUpdateCommand);

container.bind<OVHClient>(TYPES.OVHClient).to(OVHClient);
container.bind<IpifyClient>(TYPES.IpifyClient).to(IpifyClient);
container.bind<DdnsUpdateManager>(TYPES.DdnsUpdateManager).to(DdnsUpdateManager);
container.bind<DdnsIpUpdater>(TYPES.DdnsIpUpdater).to(DdnsIpUpdater);
container.bind<LoggerInterface>(TYPES.LoggerInterface).toConstantValue(logger);

export { container };
