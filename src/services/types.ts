
const TYPES = {
  CommandInterface: Symbol.for("CommandInterface"),
  IpifyClient: Symbol.for("IpifyClient"),
  OVHClient: Symbol.for("OVHClient"),
  LoggerInterface: Symbol.for("LoggerInterface"),
  DdnsUpdateManager: Symbol.for("DdnsUpdateManager"),
  DdnsIpUpdater: Symbol.for(("DdnsIpUpdater"))
};

export { TYPES };
