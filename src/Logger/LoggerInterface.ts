export interface LoggerInterface {
  debug(str: string|object): void;
  info(str: string|object): void;
  log(str: string|object): void;
  warn(str: string|object): void;
  error(str: string|object): void;
}
